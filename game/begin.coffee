Story = require('inkjs').Story
musgrave = require('./ink/fogg.json')

class SimpleGame
  game: null
  constructor: () ->
    @game = new Phaser.Game(800, 600, Phaser.AUTO, 'graphic',
      {preload: this.preload, create: this.create})
  preload: () ->
    @game.stage.backgroundColor = '#ffffff'
    @game.load.image('logo', 'img/logo.png')  
  create: () ->
    logo = @game.add.sprite(@game.world.centerX, @game.world.centerY, 'logo')  
    logo.anchor.setTo(0.5, 0.5)  

continueToNextChoice = (s) ->
  while (s.canContinue)
    paragraphs = s.Continue().split("\n")
    delay = 1000
    for i in paragraphs
      if i != ""
        html = $.parseHTML(i)
        $("#text").append($('<p>').hide().html(html).fadeIn(delay))
        delay += 500
  scrollTo = $('#options').offset().top
  if (s.currentChoices.length > 0)
    $("#options").html("").hide()
    for choice in s.currentChoices
      $("#options").append("<li><a href='#' id='choice-#{choice.index}' data-index=#{choice.index}>#{choice.text}</a></li>")
    $("#options").fadeIn(800)
    $("#options li a").click(() ->
      s.ChooseChoiceIndex($(this).data("index"))
      continueToNextChoice(s)
      return false
    )
  else
    $("#text").append("<p>THE END</p>")
    $("#options").html("")
  $('html, body').animate({
    scrollTop: scrollTo
  }, 800)

$(document).ready(() ->
  game = new SimpleGame()  
  inkStory = new Story(musgrave)
  continueToNextChoice(inkStory)
)
