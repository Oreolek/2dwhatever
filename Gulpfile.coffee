watchify    = require('watchify')
browserify  = require('browserify')
browserSync = require('browser-sync')
gulp        = require('gulp')
source      = require('vinyl-source-stream')
gutil       = require('gulp-util')
coffeify    = require('coffeeify')
coffee      = require("gulp-coffee")
sass        = require('gulp-sass')
uglify      = require('gulp-uglify')
buffer      = require('vinyl-buffer')
zip         = require('gulp-zip')
concat      = require('gulp-concat')
shell       = require('gulp-shell')
rename      = require('gulp-rename')
fs = require 'fs'
CSON = require 'cson'
glob = require 'glob'

reload = browserSync.reload

html = (target) ->
  return () ->
    return gulp.src(['html/*.html']).pipe(gulp.dest(target))

# Images
img = (target) ->
  return () ->
    return gulp.src(['img/*.png', 'img/*.jpeg', 'img/*.jpg']).pipe(gulp.dest(target))

# Audio assets
audio = (target) ->
  return () ->
    return gulp.src(['audio/*.mp3']).pipe(gulp.dest(target))

gulp.task('html', html('./build'))
gulp.task('img', img('./build/img'))
gulp.task('audio', audio('./build/audio'))

gulp.task('sass', () ->
  gulp.src('sass/main.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./build/css'))
)

bundler = watchify(browserify({
  entries: ["./build/game/main.coffee"]
  debug: true
  transform: [coffeify]
}))

gulp.task('ink', () ->
  return true # at the moment i don't have a working standalone inklecate
  return gulp.src([
    'game/ink/*.ink'
  ])
  .pipe(gulp.dest('build/game/ink'))
  .pipe(shell([
    'inklecate <%= file.path %>'
  ]))
)

bundle = () ->
  return bundler.bundle()
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('./build/game'))

gulp.task('concatCoffee', () ->
  ###
  for language in ['ru', 'en']
    files = glob.sync('game/procgen/'+language+'/*.cson')
    spec = {}
    for file in files
      data = CSON.parseCSONFile(file)
      if not data.groups?
        data.groups = []
      if data.phrases?
        data.groups.push({
          tags: [],
          phrases: data.phrases
        })
        data.phrases = undefined
      key = file.substr(0, file.lastIndexOf('.')) || file
      key = key.replace('game/procgen/'+language+'/', '')
      spec[key] = data

    json = JSON.stringify(spec) + '\n'
    fs.writeFileSync("./build/game/procgen/"+language+".json", json)
  ###

  return gulp.src([
    './game/begin.coffee',
    #'./game/story.coffee',
  ]).pipe(concat('./main.coffee')).pipe(gulp.dest('./build/game'))
)

gulp.task('coffee', ['concatCoffee'], bundle)

bundler.on('update', bundle)
bundler.on('log', gutil.log)

gulp.task('build', ['html', 'img', 'sass', 'coffee', 'audio', 'ink'])

gulp.task('serve', ['build'], () ->
  browserSync({
    server: {
      baseDir: 'build'
    }
    browser: []
  })

  sassListener = () ->
    reload('./build/css/main.css')

  gulp.watch(['./html/*.html'], ['html'])
  gulp.watch(['./sass/*.scss'], ['sass'])
  gulp.watch(['./img/*.png', './img/*.jpeg', './img/*.jpg'], ['img'])
  gulp.watch(['./game/*.coffee'], ['coffee']);
  gulp.watch(['./game/procgen/*/*.cson'], ['concatCoffee']);

  gulp.watch(['./build/css/main.css'], sassListener)
  gulp.watch(
    ['./build/game/bundle.js', './build/img/*', './build/index.html'],
    browserSync.reload)
)

gulp.task('html-dist', html('./dist'))
gulp.task('img-dist', img('./dist/img'))
gulp.task('audio-dist', audio('./dist/audio'))
gulp.task('legal-dist', () ->
  return gulp.src(['LICENSE.txt'])
    .pipe(gulp.dest("./dist"))
)

gulp.task('sass-dist', () ->
  return gulp.src('./sass/main.scss')
    .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('./dist/css'))
)

distBundler = browserify({
  debug: false,
  entries: ['./build/game/main.coffee'],
  transform: ['coffeeify']
})

gulp.task('coffee-dist', ['concatCoffee'], () ->
  return distBundler.bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(uglify())
    .on('error', gutil.log)
    .pipe(gulp.dest('./dist/game'))
)

gulp.task('dist', [
  'html-dist',
  'img-dist',
  'sass-dist',
  'coffee-dist',
  'audio-dist',
  'legal-dist',
  'ink'
], () ->
  gulp.src([
    './build/game/ink/*.json'
  ]).pipe(gulp.dest("./dist/game/ink"))
  gulp.src([
    './build/game/procgen/*.json'
  ]).pipe(gulp.dest("./dist/game/procgen"))
  return true
)

gulp.task('zip', ['dist'], () ->
  return gulp.src('dist/**')
    .pipe(zip('dist.zip'))
    .pipe(gulp.dest('.'))
)
